# Οργάνωση του συνεδρίου FOSSCOMM 2021

Το αποθετήριο αυτό εξυπηρετεί την οργάνωση του συνεδρίου FOSSCOMM 2021 από το Τμήμα Εφαρμοσμένης Πληροφορικής του Πανεπιστημίου Μακεδονίας.

**Επιστημονικός υπεύθυνος:**   
Αναπλ. Καθηγητής Άγγελος Σιφαλέρας ([http://sites.uom.gr/sifalera/](http://sites.uom.gr/sifalera/))

**Φοιτητές τμήματος:**  
* Παρασκευάς Γεώργιος Μπακάλης ([Linkedin](https://www.linkedin.com/in/paraskevas-georgios-bakalis-6785591a9/))  
* Γεώργιος Μαμιδάκης ([Linkedin](https://www.linkedin.com/in/gmamidak/)) 
* Ελένη Κουβαλακίδου  ([Linkedin](https://www.linkedin.com/in/eleni-kouvalakidou-0252a3209/))
* Χρήστος Γιαμακίδης  
* Κωνσταντίνος Θωμασιάδης  
* Ευαγγελία Δανδίκα  
* Βασίλης Τζεβελέκος  ([Linkedin](https://www.linkedin.com/in/vasilis-tzevelekos-800908209/))
* Γεωργία Σβάρνα  
* Χρύσανθος Μπουρουτζόγλου ([Linkedin](https://www.linkedin.com/in/chrysanthos-bouroutzoglou-1b4515209/)) 
* Ευστάθιος Ιωσηφίδης ([Linkedin](https://www.linkedin.com/in/eiosifidis/))  
